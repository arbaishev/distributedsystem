package ru.albert.worker;

public interface Service {

    void start();
    void stop();
    boolean isRunning();
}
