package ru.albert.worker.utils.download;

import ru.albert.worker.exception.DownloadException;

public interface Downloader {
    void download(String url, String dstFilePath) throws DownloadException;
}
