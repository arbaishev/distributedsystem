package ru.albert.worker.connector;

public interface StorageLayerConnector extends Runnable {
    void stop();
    long getStat();
}
