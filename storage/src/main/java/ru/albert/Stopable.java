package ru.albert;

public interface Stopable extends Runnable {
    void stop();
}
