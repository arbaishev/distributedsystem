package ru.albert.utils.download;


import ru.albert.exception.DownloadException;

public interface Downloader {
    void download(String url, String dstFilePath) throws DownloadException;
}
