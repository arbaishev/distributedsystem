package ru.albert.storage.receiver.processors;

import ru.albert.storage.data.WorkerTaskResult;

public interface SentenceProcessor {
    WorkerTaskResult decode(String sentence);
}
