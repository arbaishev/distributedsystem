package ru.albert.storage.receiver.processors;

import com.google.gson.Gson;
import ru.albert.storage.data.WorkerTaskResult;

public class JsonSentenceProcessor implements SentenceProcessor {
    private Gson gson;

    public JsonSentenceProcessor() {
        gson = new Gson();
    }

    @Override
    public WorkerTaskResult decode(String sentence) {
        return gson.fromJson(sentence, WorkerTaskResult.class);
    }
}
