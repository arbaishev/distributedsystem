package ru.albert.manager.db.repositories;

import ru.albert.manager.db.entity.StorageConfig;
import org.springframework.data.repository.CrudRepository;

public interface StorageConfigRepository extends CrudRepository<StorageConfig, Long> {
}
