package ru.albert.manager.db;

public enum NodeType {
    WORKER,
    STORAGE
}