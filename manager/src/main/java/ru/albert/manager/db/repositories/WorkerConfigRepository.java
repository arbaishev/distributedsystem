package ru.albert.manager.db.repositories;

import ru.albert.manager.db.entity.WorkerConfig;
import org.springframework.data.repository.CrudRepository;

public interface WorkerConfigRepository extends CrudRepository<WorkerConfig, Long> {
}
