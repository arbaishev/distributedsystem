package ru.albert.manager.db.repositories;

import ru.albert.manager.db.entity.StorageLayer;
import org.springframework.data.repository.CrudRepository;

public interface StorageLayerRepository extends CrudRepository<StorageLayer, Long> {
    StorageLayer findByAddress(String address);
}
