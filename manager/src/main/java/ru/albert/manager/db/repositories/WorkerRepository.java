package ru.albert.manager.db.repositories;

import ru.albert.manager.db.entity.Worker;
import org.springframework.data.repository.CrudRepository;

public interface WorkerRepository extends CrudRepository<Worker, Long> {
    Worker findByAddress(String address);
}
