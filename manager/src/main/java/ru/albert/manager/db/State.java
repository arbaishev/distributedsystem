package ru.albert.manager.db;

public enum State {
    RUN,
    STOP,
    DEAD
}